package cest.pet;

import cest.pet.TipoRaca;;

public abstract class Animal {
	
		private String nome;
		private int idade;
		private TipoRaca raca;
		private boolean doente;
		
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public int getIdade() {
			return idade;
		}
		public void setIdade(int idade) {
			this.idade = idade;
		}
		public TipoRaca getRaca() {
			return raca;
		}
		public void setRaca(TipoRaca raca) {
			this.raca = raca;
		}
		public boolean isDoente() {
			return doente;
		}
		public void setDoente(boolean doente) {
			this.doente = doente;
		}
		


	

}
